package edu.westga.smarttherm.model;

import java.time.Clock;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Represents a smart thermostat controller that minimizes energy consumption and maximizes comfort based on user preferences.
 * 
 * @author lewisb
 *
 */
public class SmartThermostat {
	private ArrayList<SetPoint> setPoints;
	private int desiredTemp;
	private Clock clock;
	private IHvacSystem hvac;
	private IThermometer thermometer;
	
	/**
	 * Factory default temperature
	 */
	public static final int DEFAULT_TEMP = 65;
	
	/**
	 * The minimum temperature to which we can cool.
	 */
	public static final int TEMP_MIN = 32;
	
	/**
	 * The maximum temperature to which we can heat.
	 */
	public static final int TEMP_MAX = 120;
	
	
	/**
	 * Creates a new SmartThermostat using the given thermometer.
	 */
	public SmartThermostat() {
		this.setPoints = new ArrayList<SetPoint>();
		this.desiredTemp = DEFAULT_TEMP;
		this.clock = Clock.systemUTC();
	}
	
	/**
	 * Clears the schedule of all set points.  The current desired temperature is retained, however.
	 */
	public void clearSchedule() {
		this.setPoints.clear();
	}
		
	/**
	 * Sets the thermostat to the desired temperature and notifies the SmartThermostat
	 * that this is the desired temperature for this time of day.  Will not set lower than TEMP_MIN or higher
	 * than TEMP_MAX
	 * 
	 * @param temp the desired temperature
	 */
	public void setDesiredTemperature(int temp) {
		if (temp < TEMP_MIN) {
			temp = TEMP_MIN;
		}
		
		if (temp > TEMP_MAX) {
			temp = TEMP_MAX;
		}
		
		this.desiredTemp = temp;
		SetPoint setPoint = new SetPoint(temp, LocalTime.now(this.clock));
		this.setPoints.add(setPoint);
		Collections.sort(this.setPoints);
	}
	
	/**
	 * Initiates one round of measuring the temperature and taking appropriate action
	 * with the HVAC system.
	 */
	public void measureTempAndOperateHVAC() {
		if (this.hvac == null) {
			throw new IllegalStateException("hvac may not be null");
		}
		
		if (this.thermometer == null) {
			throw new IllegalStateException("thermometer may not be null");
		}
		
		int currentTemp = this.thermometer.getTemperature();
		
		if (currentTemp > this.desiredTemp) {
			this.hvac.cool();
		} else if (currentTemp < this.desiredTemp) {
			this.hvac.heat();
		} else {
			this.hvac.turnOff();
		}
	}
	
	/**
	 * Gets the desired temperature
	 * @return the desired temperature
	 */
	public int getDesiredTemperature() {
		return this.desiredTemp;
	}
	
	/**
	 * Gets the daily schedule of set points, ordered from earliest to latest beginning at midnight.
	 * @return the daily schedule of set points
	 */
	public SetPoint[] getSchedule() {
		return this.setPoints.toArray(new SetPoint[0]);
	}
	
	/**
	 * Sets the thermometer for this thermostat
	 * 
	 * @param thermometer the thermometer to use
	 */
	public void setThermometer(IThermometer thermometer) {
		if (thermometer == null) {
			throw new IllegalArgumentException("thermometer may not be null");
		}
		this.thermometer = thermometer;
	}
	
	/**
	 * Sets the HVAC system for this thermostat
	 * 
	 * @param hvac the HVAC system to use
	 */
	public void setHvacSystem(IHvacSystem hvac) {
		if (hvac == null) {
			throw new IllegalArgumentException("hvac may not be null");
		}
		this.hvac = hvac;
	}
}
