package edu.westga.smarttherm.model.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import edu.westga.smarttherm.model.SmartThermostat;


public class WhenChangingDesiredTemperature {
	
	private SmartThermostat st;

	@Before
	public void setUp() throws Exception {
		this.st = new SmartThermostat();
	}

	@Test
	public void shouldSetAtTempMin() {
		this.st.setDesiredTemperature(SmartThermostat.TEMP_MIN);
		assertEquals(SmartThermostat.TEMP_MIN, this.st.getDesiredTemperature());
	}
	
	@Test
	public void tempLowerThanTempMinShouldSetAtTempMin() {
		this.st.setDesiredTemperature(SmartThermostat.TEMP_MIN - 1);
		assertEquals(SmartThermostat.TEMP_MIN, this.st.getDesiredTemperature());
		
		this.st.setDesiredTemperature(SmartThermostat.TEMP_MIN - 10);
		assertEquals(SmartThermostat.TEMP_MIN, this.st.getDesiredTemperature());
	}

	@Test
	public void shouldSetAtTempMax() {
		this.st.setDesiredTemperature(SmartThermostat.TEMP_MAX);
		assertEquals(SmartThermostat.TEMP_MAX, this.st.getDesiredTemperature());
	}
	
	@Test
	public void tempHigherthanTempMaxShouldSetAtTempMax() {
		this.st.setDesiredTemperature(SmartThermostat.TEMP_MAX + 1);
		assertEquals(SmartThermostat.TEMP_MAX, this.st.getDesiredTemperature());
		
		this.st.setDesiredTemperature(SmartThermostat.TEMP_MAX + 10);
		assertEquals(SmartThermostat.TEMP_MAX, this.st.getDesiredTemperature());
	}
	
	@Test
	public void shouldSetBetweenTempMinAndTempMax() {
		int expected = SmartThermostat.TEMP_MIN + (SmartThermostat.TEMP_MAX - SmartThermostat.TEMP_MIN)/2;
		this.st.setDesiredTemperature(expected);
		assertEquals(expected, this.st.getDesiredTemperature());
	}
	
	@Test
	public void desiredTempShouldAlwaysBeLatestSetTemp() {
		this.st.setDesiredTemperature(78);
		this.st.setDesiredTemperature(65);
		this.st.setDesiredTemperature(60);
		this.st.setDesiredTemperature(71);
		int expected = 72;
		this.st.setDesiredTemperature(expected);
		assertEquals(expected, this.st.getDesiredTemperature());
	}
}
