package edu.westga.smarttherm.model.tests;

import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import edu.westga.smarttherm.model.IHvacSystem;
import edu.westga.smarttherm.model.IThermometer;
import edu.westga.smarttherm.model.SmartThermostat;

@RunWith(EasyMockRunner.class)
public class WhenControllingTemperature {

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	private SmartThermostat st;
	private IThermometer thermometer;
	private IHvacSystem hvacSystem;

	@Before
	public void setUp() throws Exception {
		st = new SmartThermostat();
		thermometer = EasyMock.createStrictMock(IThermometer.class);
		hvacSystem = EasyMock.createStrictMock(IHvacSystem.class);
	}

	@Test()
	public void TestWhenHVACisNull() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("hvac may not be null");

		st.setThermometer(thermometer);
		st.measureTempAndOperateHVAC();
	}

	@Test
	public void TestWhenThermometerIsNull() {
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("thermometer may not be null");

		st.setHvacSystem(hvacSystem);
		st.measureTempAndOperateHVAC();
	}

	@Test
	public void TestCoolWhenCurrentTempGreaterThanDesiredTemp() {
		st.setHvacSystem(hvacSystem);
		st.setThermometer(thermometer);

		st.setDesiredTemperature(68);
		EasyMock.expect(thermometer.getTemperature()).andReturn(72);

		hvacSystem.cool();

		EasyMock.replay(hvacSystem);
		EasyMock.replay(thermometer);

		st.measureTempAndOperateHVAC();

		EasyMock.verify(hvacSystem);
		EasyMock.verify(thermometer);
	}

	@Test
	public void TestHeatWhenCurrentTempLessThanDesiredTemp() {
		st.setHvacSystem(hvacSystem);
		st.setThermometer(thermometer);

		st.setDesiredTemperature(72);
		EasyMock.expect(thermometer.getTemperature()).andReturn(68);

		hvacSystem.heat();

		EasyMock.replay(hvacSystem);
		EasyMock.replay(thermometer);

		st.measureTempAndOperateHVAC();

		EasyMock.verify(hvacSystem);
		EasyMock.verify(thermometer);

	}

	@Test
	public void TestTurnOffWhenCurrentTempIsDesiredTemp() {
		st.setHvacSystem(hvacSystem);
		st.setThermometer(thermometer);

		st.setDesiredTemperature(68);
		EasyMock.expect(thermometer.getTemperature()).andReturn(68);

		hvacSystem.turnOff();

		EasyMock.replay(hvacSystem);
		EasyMock.replay(thermometer);

		st.measureTempAndOperateHVAC();

		EasyMock.verify(hvacSystem);
		EasyMock.verify(thermometer);

	}

}
