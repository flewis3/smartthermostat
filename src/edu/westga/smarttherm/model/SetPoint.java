package edu.westga.smarttherm.model;

import java.time.LocalTime;

/**
 * A SetPoint represents a desired temperature and a datetime at which that temperature is desired.
 * 
 * @author lewisb
 *
 */
public final class SetPoint implements Comparable<SetPoint> {
	private int temp;
	private LocalTime whenScheduled;
	
	
	
	/**
	 * Creates a SetPoint with the given temperature at the scheduled time
	 * 
	 * @param temp the temperature desired
	 * @param whenScheduled the time we desire that temperature
	 */
	public SetPoint(int temp, LocalTime whenScheduled) {
		if (whenScheduled == null) {
			throw new IllegalArgumentException("timestamp may not be null");
		}
		
		this.temp = temp;
		this.whenScheduled = whenScheduled;
	}
	
	/**
	 * Gets the temperature desired by this SetPoint
	 * @return the temperature desired by this SetPoint
	 */
	public int getTemp() {
		return this.temp;
	}
	
	/**
	 * Gets the time this temperature is scheduled for
	 * @return the time this temperature is scheduled for
	 */
	public LocalTime getScheduledTime() {
		return this.whenScheduled;
	}

	@Override
	public int compareTo(SetPoint other) {
		return this.whenScheduled.compareTo(other.whenScheduled);
	}
}
