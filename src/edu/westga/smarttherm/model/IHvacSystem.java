package edu.westga.smarttherm.model;

/**
 * HvacSystem defines the methods required to heat and cool.
 * 
 * @author lewisb
 *
 */
public interface IHvacSystem {
	/**
	 * Turns on the heating sub-system;
	 */
	void heat();
	
	/**
	 * Turns on the A/C sub-system;
	 */
	void cool();
	
	/**
	 * Turns off any and all heating and cooling.
	 */
	void turnOff();
}
